const deferredPrompt;

if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register('/service-worker.js') // tham số thứ 2 là object dùng để scope file sử dụng
		.then(function() {
			console.log('Service worker registered!');
		});
}
window.addEventListener('beforeinstallprompt', function(event) {
	console.log('beforeinstallprompt fired');
	event.preventDefault();
	deferredPrompt = event;
	return false;
});