import React, { Suspense } from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";

const LazyHome = React.lazy(() => import("./components/Home"));
const LazyAbout = React.lazy(() => import("./components/About"));

function App() {
  const handleInstall = () => {
    if (deferredPrompt) {
      deferredPrompt.prompt();
      deferredPrompt.userChoice.then(function(choiceResult) {
        console.log(choiceResult.outcome);

        if (choiceResult.outcome === "dismissed") {
          console.log("User cancel installation");
        } else {
          console.log("User added to home screen");
        }
      });

      deferredPrompt = null;
    }
  };
  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <BrowserRouter>
          <div>
            <Route path="/" exact component={LazyHome} />
            <Route path="/about" exact component={LazyAbout} />
          </div>
        </BrowserRouter>
      </Suspense>
      <button onClick={handleInstall}>Click Install</button>
    </div>
  );
}

export default App;
